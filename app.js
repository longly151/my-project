const express = require ('express');

const morgan = require('morgan');

const mongoose = require('mongoose');

const { port,db,sessionSecret} = require ('./config/config');

const bodyParser = require ('body-parser');

const router = require('./routers');

const session = require('express-session');

const flash = require('connect-flash');

const mongoStore = require('connect-mongo')(session);




const app = express();

app.use(flash());
app.use(session({
    name: 'blog',
    secret: sessionSecret,
    resave: false,
    saveUninitialized: true,
    store: new mongoStore({url : db}),
}));

mongoose.connect(db,{ useNewUrlParser: true });
mongoose.connection.on('error',(err)=>{
    console.log('>>> error database',err);
});
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.set ('views', './views');

app.use('/public', express.static('./public'));

app.use(morgan('dev'));

app.use(router)


app.listen (port,(err) =>{
    if (err){
        console.log ('Lỗi server');
        return;
    }
    console.log ('Server running on port',port);
});