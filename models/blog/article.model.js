const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const getSlug = require('speakingurl');

const articleSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    content: {
        type: String,
        required: true,
        trim: true,
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'users',
    },
    url: {
        type: String,
        unique: true,
        trim: true,
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'categories',
    }


}, {
    timestamps: true
});

articleSchema.pre('save', function (next) {
    const article = this;
    if (!article.isModified('title')) return next();
    const url = getSlug(article.title, {
        lang: 'vn',
    });
    const month = article.updatedAt.getMonth() + 1;
    const time = "" + article.updatedAt.getFullYear() + "0" + month + "0" + article.updatedAt.getDate() + article.updatedAt.getHours() + article.updatedAt.getMinutes() + article.updatedAt.getSeconds()
    article.url = url + time;
    next();
});
module.exports = mongoose.model('articles', articleSchema);