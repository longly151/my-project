const mongoose = require('mongoose');
const bcrypt= require('bcryptjs');
const Schema = mongoose.Schema;


const userSchema = new Schema({
    username:{
        type: String,
        unique: true,
        required: true
    },
    email:{
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        required: true,
    },
    password:{
        type: String,
        required: true,
        
    },
    avatar:{
        type:String,
        default:'https://i.pinimg.com/564x/1e/9f/a1/1e9fa1bb13793eee8f358ec30135ad84.jpg'
    },
},{
    timestamps: true,
});

userSchema.pre('save',function(next){
    const user = this;
    if(!user.isModified('password')) return next();

    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(user.password, salt, (err, hash)=> {
            if (err) return next(err);
            user.password=hash;
            next();
        });
    });
});


module.exports = mongoose.model('users',userSchema);