const {check, validationResult} = require('express-validator/check');

const checkPost = [
    check('title').trim().not().isEmpty().withMessage('Title không được để trống'),
    check('content').trim().not().isEmpty().withMessage('Nội dung không được để trống'),
    check('category').trim().not().isEmpty().withMessage('Category không được để trống')
]; 

const checkCategory = [
    check('name').trim().not().isEmpty().withMessage('Tên chuyên mục không được để trống'),
];
module.exports = {
    checkPost,
    checkCategory
}