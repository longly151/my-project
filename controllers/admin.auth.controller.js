const bcrypt = require ('bcryptjs');
const {
    check,
    validationResult
} = require('express-validator/check');
const User = require('../models/users/user.model');


const registerController=(req, res) => {
    const {
        username,
        email,
        password
    } = req.body;
    const cUser = new User({
        username,
        email,
        password,
    });
    cUser.save((err, user) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log('>>> User: ', user);
        res.redirect('/admin/login');

    });
};
const loginController = (req, res)=>{
    const {
        username,
        password
    } = req.body;
    User.findOne({
        username: username
    }, (err, user) => {
        if (err) {
            console.log(err);
            return;
        }
        if (user) {
            if (bcrypt.compareSync(password, user.password)) {
                req.session.user ={
                    _id: user._id,
                    username: user.username,
                    avatar: user.avatar
                    
                }
                req.flash('success','Welcome ',user.username);
                res.redirect('/admin');
            } else {
                console.log('Sai mat khau');
                req.flash('danger','Mật khẩu sai');
                res.redirect('/admin/login');
            }
        } else {
            console.log("Khong tim thay user");
            req.flash('danger','Không tìm thấy tên đăng nhập');
            res.redirect('/admin/login');
        }
    });
};

const logoutController = (req, res)=>{
    req.session.destroy((err)=>{
        if(err){
            console.log(err);
            return;
        }
        res.redirect('/admin/login');
    });
};
module.exports ={
    loginController,
    registerController,
    logoutController
}
