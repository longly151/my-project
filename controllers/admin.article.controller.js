const {
    check,
    validationResult
} = require('express-validator/check');
const Article = require('../models/blog/article.model');
const Category = require('../models/blog/category.model');

const middlewareModify = (req, res, next) => {
    const articleId = req.params.id;
    const userId = req.session.user._id;
    Article
        .findById(articleId)
        .exec((err, article) => {
            if (err) {
                return next(err);
            };
            if (!article) return next(new Error('Article not found'));
            if (article.author.toString() !== userId) {
                req.flash('danger', 'Access denied')
                return res.redirect('/admin/article/manage/1');
            };
            next();
        });

};

const getAddArticle = (req, res) => {
    Category
        .find()
        .select('_id name')
        .sort({
            name: 1
        })
        .exec((err, categories) => {
            if (err) {
                const error = new Error('Internal Server Error');
                error.status = 500;
                return;
            }
            res.render('admin/add', {
                categories
            });
        });
};
const postAddArticle = (req, res) => {
    const {
        title,
        description,
        content,
        author,
        category
    } = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        errors.array().forEach((err) => {
            req.flash(err.param, err.msg);
        });

        req.flash('objBody', {
            title,
            description,
            content,
            author,
            category
        });

        return res.redirect('/admin/article/add');

    };

    const article = new Article({
        title,
        description,
        content,
        author,
        category
    });
    article.save((err) => {
        if (err) {
            console.log('>>>ERROR: ', err);
            return;
        }
        res.redirect('/admin/article/manage-by-author/1');
    });
};
const getArticles = (req, res, next) => {
    const perPage = 4; /* perPage - số dòng dữ liệu trên mỗi trang */
    const page = req.params.page || 1; /* page - biến chứa số trang hiện tại (Lấy từ request) */
    Article
        .find({})
        .populate('author', '-_id username')
        .populate('category', '-_id name')
        .sort({
            updatedAt: -1
        })
        .select('title description content createdAt updatedAt author category url')
        .skip((perPage * page) - perPage) /* mỗi trang chúng ta cần phải bỏ qua ((perPage * page) - perPage) giá trị (trên trang đầu tiên giá trị của bỏ qua phải là 0): */
        .limit(perPage)
        .exec(function (err, articles) {
            Article.count().exec(function (err, count) { /* dùng count để tính số trang */
                if (err) throw err;
                res.render('admin/manage', { /* hiển thị và gửi dữ liệu đi kèm */
                    articles,
                    current: page,
                    pages: Math.ceil(count / perPage)
                });
            });
        });
};

const deleteArticle = (req, res) => {
    Article.deleteOne({
        _id: req.params.id
    }, (err) => {
        if (err) {
            console.log(err);
            req.flash('danger', 'Delete article failed');
            return res.redirect('/admin/article/add');
        }

        req.flash('success', 'Delete Article successfully');
        res.redirect('/admin/article/manage-by-author/1');
    });
};

const getArticle = (req, res) => {
    Article
        .findById(req.params.id)
        .exec((err, article) => {
            if (err) {
                console.log(err);
                return;
            };
            if (!article) {
                req.flash('danger', 'Can not found article');
                return res.redirect('/admin/article/manage-by-author/1');
            };
            Category
                .find()
                .select('_id name')
                .sort({
                    name: 1
                })
                .exec((err, categories) => {
                    if (err) {
                        const error = new Error('Internal Server Error');
                        error.status = 500;
                        return;
                    }
                    res.render('admin/edit', {
                        article,
                        categories
                    });
                });
        });
};

const editArticle = (req, res) => {
    const {
        title,
        description,
        content,
        category
    } = req.body;
    const {
        id
    } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        errors.array().forEach((err) => {
            req.flash(err.param, err.msg);
        });

        req.flash('objBody', {
            title,
            description,
            content,
            category
        });
        return res.redirect(`/admin/article/edit/${id}`);
    };

    Article.findById({
        _id: id
    }, (err, article) => {
        if (err) {
            console.log(err);
            return;
        };
        if (!article) {
            req.flash('danger', 'Can not found article');
            return res.redirect('/admin/article/manage-by-author/1');
        };
        article.title = title;
        article.description = description;
        article.content = content;
        article.category = category;
        article.save((err, article) => {
            if (err) {
                console.log('>>>ERROR: ', err);
                req.flash('danger', 'Can not edit article');
                return res.redirect(`/admin/article/edit/${id}`);
            }
            res.redirect('/admin/article/manage-by-author/1');
        });

    });
};

const getArticlesByAuthor = (req, res) => {
    const authorid = req.session.user._id;
    const perPage = 4; /* perPage - số dòng dữ liệu trên mỗi trang */
    const page = req.params.page || 1; /* page - biến chứa số trang hiện tại (Lấy từ request) */
    Article
        .find({
            author: authorid
        })
        .populate('author', '-_id username')
        .populate('category', '-_id name')
        .sort({
            updatedAt: -1
        })
        .select('title description content createdAt updatedAt author category url')
        .skip((perPage * page) - perPage) /* mỗi trang chúng ta cần phải bỏ qua ((perPage * page) - perPage) giá trị (trên trang đầu tiên giá trị của bỏ qua phải là 0): */
        .limit(perPage)
        .exec(function (err, articles) {
            Article
                .find({
                    author: authorid
                })
                .count()
                .exec(function (err, count) { /* dùng count để tính số trang */
                    if (err) throw err;
                    res.render('admin/manage-by-author', { /* hiển thị và gửi dữ liệu đi kèm */
                        articles,
                        current: page,
                        pages: Math.ceil(count / perPage)
                    });
                });
        });
};

module.exports = {
    middlewareModify,
    getAddArticle,
    postAddArticle,
    deleteArticle,
    getArticle,
    editArticle,
    getArticles,
    getArticlesByAuthor,
};