const express = require('express');
const router = express.Router();
const {
    getAllArticles,
    getArticleByUrl
} = require('../controllers/client.article.controller');


router.get('/', (req, res,next) => {
    getAllArticles(req, res, next);
});

router.get('/about', (req, res) => {
    res.render('client/about', {
        img: 'about',
        title: 'About Me',
        note: 'This is what I do.'
    });
});

router.get('/post/:url', (req, res,next) => {
    getArticleByUrl(req, res, next);
});

router.get('/contact', (req, res) => {
    res.render('client/contact', {
        img: 'contact',
        title: 'Contact Me',
        note: 'Have questions? I have answers.'
    });
});




module.exports = router;