const express = require('express');
const router = express.Router();
const {
    checkPost
} = require('../config/validate');

const {
    middlewareModify,
    getAddArticle,
    postAddArticle,
    deleteArticle,
    getArticle,
    editArticle,
    getArticles,
    getArticlesByAuthor,
} = require('../controllers/admin.article.controller');

router.use(['/delete/:id','/edit/:id'],(req, res, next)=>{
    middlewareModify(req,res,next);
});


router.route('/add')
    .get((req, res) => {
        getAddArticle(req, res);
    })
    .post(checkPost,(req, res)=>{
        postAddArticle(req, res);
    });

router.get('/delete/:id',(req, res)=>{
    deleteArticle(req, res);
});
router.route('/edit/:id')
    .get((req, res)=>{
        getArticle(req, res);
    })
    .post(checkPost,(req, res)=>{
        editArticle(req, res);
    });

router.get('/manage/:page', (req, res) => {
    getArticles(req, res);
});
router.get('/manage-by-author/:page',(req, res)=>{
    getArticlesByAuthor(req, res);
});

module.exports = router;