const express = require('express');
const router = express.Router();

const {
    loginController,
    registerController,
    logoutController
} = require('../controllers/admin.auth.controller');



router.use((req, res, next) => {
    res.locals.flash_messages = req.session.flash;
    delete req.session.flash;
    next();
});

router.use(['/login', '/register'], (req, res, next) => {
    if (req.session.user) {
        return res.redirect('/admin');
    }
    return next();
})

router.get('/login', (req, res) => {
    res.render('admin/login');
});

router.post('/register', (req, res) => {
    registerController(req,res);
});

router.post('/login', (req, res) => {
    loginController(req,res);
});

router.use((req, res, next) => {
    if (req.session.user) {
        res.locals.cUser = req.session.user;
        return next();
    }
    req.flash('danger', 'Hack à ?');
    res.redirect('/admin/login');
});

router.get('/', (req, res) => {
    res.render('admin/index');
});



router.get('/logout', (req, res) => {
    logoutController(req,res);
});

module.exports = router;